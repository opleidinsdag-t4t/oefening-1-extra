﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T4T.Opleidingsdag.Producer.Events
{
    public class ShoppingBasketCreated
    {
        public Guid ShoppingBasketId { get; set; }
        public Guid UserId { get; set; }
    }
}
