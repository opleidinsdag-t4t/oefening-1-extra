﻿using System;
using SqlStreamStore;
using SqlStreamStore.Streams;

namespace T4T.Opleidingsdag.RealTimeConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var store = new MsSqlStreamStore(new MsSqlStreamStoreSettings("Server=.;Database=T4T.Opleidingsdag.Oefening1;Trusted_Connection=True;"));

            if (!store.CheckSchema().Result.IsMatch())
                store.CreateSchema().Wait();

            var streamId = new StreamId("Foo");

            Console.WriteLine("Subscribing to Stream Foo...");

            store.SubscribeToStream(
                streamId: streamId,
                continueAfterVersion: 0,
                streamMessageReceived: async (subscription, message, token) =>
                {
                    var data = await message.GetJsonData(token);
                    Console.WriteLine($"Message received: {message.Type}. Data: {data}");
                });

            Console.ReadKey();
        }
    }
}
